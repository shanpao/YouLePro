/**
 * Created by Administrator on 2015/12/20.
 */
var appFunc = {
    currentData:null,
    select:function(){
        $(".code").change(function(){
            $("#buyCodeText").text(appFunc.currentData[$(this).val()]["name"]);
        });
    },
    getBuy:function(){
        appFunc.get("/get",{},"getBuy");
    },
    get:function(key){
        $.get("pages/"+key+".html",function(html){
            $(".right").html(html);
       })
    },
    set:function(key){

    },
    messageEdit:function(){
        layer.open({
            type: 1,
            shade: false,
            title: "员工设置", //不显示标题
            content: $('#message'), //捕获的元素
            maxWidth:480,
            modal:true,
            shade:0.6,
            cancel: function (index) {
                layer.close(index);
                this.content.show();
                //layer.msg('捕获就是从页面已经存在的元素上，包裹layer的结构', {time: 5000, icon: 6});
            }
        });
    },
    messageCancel:function(){
        layer.confirm('确定删除么？', {
            btn: ['确定','取消'] //按钮
        }, function(){
            layer.msg('删除成功', {icon: 1});
        }, function(){
            /*layer.msg('也可以这样', {
                time: 20000, //20s后自动关闭
                btn: ['明白了', '知道了']
            });*/
        });
    },
    createDom:function($dom){
        var html = "";
        $.each(appFunc.currentData,function(i){
            html+="<option value='"+ i +"' name='"+ appFunc.currentData[i]["name"] +"'>"+ appFunc.currentData[i]['code'] +"</option>";
        });
        $dom.append($(html))
    }
}
$(function(){
    $("#content").delegate(".edit","click",function(){
        appFunc.messageEdit();
    });
    $("#content").delegate(".delete","click",function(){
        appFunc.messageCancel();
    });
    $(".menu li").click(function(){
        appFunc.get($(this).attr("name"));
    });
});


